const browserify = require('browserify')
const buffer = require('vinyl-buffer')
const gulp = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const server = require('gulp-webserver')
const source = require('vinyl-source-stream')
const sourcemaps = require('gulp-sourcemaps')

gulp.task('server', () => {
  gulp.src('dist').pipe(
    server({
      livereload: true,
      open: true,
      port: 8000,
    })
  )
})

gulp.task('scss', () => {
  return gulp
    .src('src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ includePaths: ['./node_modules'] }).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css/'))
})

gulp.task('js', () => {
  return browserify({
    entries: 'src/js/main.js',
    debug: true,
  })
    .transform('babelify', {
      presets: ['@babel/preset-env'],
    })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./dist/js/'))
})

gulp.task('build', gulp.series('scss', 'js'))

gulp.task('watch', () => {
  gulp.watch('src/scss/**/*.scss', gulp.series('scss'))
  gulp.watch('src/js/**/*.js', gulp.series('js'))
})

gulp.task('default', gulp.series('build', gulp.parallel('server', 'watch')))
