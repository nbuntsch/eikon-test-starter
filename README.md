# eikon test starter

## Fonctionnalités

- Serveur de développement
    - Rafraîchissement automatique
- Compilation SCSS
    - Automatique à la modification d'un fichier
    - Sourcemaps facilitant le debuggage avec le navigateur
    - Import des styles des paquets NPM
- Aggrégation des fichiers JS
    - Automatique à la modification d'un fichier
    - Babel pour une compatibilité optimale tout en utilisant les derniers standards
    - Sourcemaps facilitant le debuggage avec le navigateur
    - Import des scripts des paquets NPM

## Prérequis

- Git installé
- NPM installé

## Installation

Cloner le repository git

```
git clone git@bitbucket.org:nbuntsch/eikon-test-starter.git <nom-du-projet>
```

Se rendre dans le dossier du projet, puis installer les dépendances avec NPM

```
cd <nom-du-projet>
npm install
```

## Commandes

Compiler la SCSS, aggréger le JS, lancer le serveur et écouter les changements

```
gulp
```

Compiler la SCSS

```
gulp scss
```

Aggréger le JS

```
gulp js
```

Compiler la SCSS et aggréger le JS

```
gulp build
```

Ecouter les changements sur les fichiers et compiler la SCSS ou aggréger le JS

```
gulp watch
```

Lancer le serveur

```
gulp server
```

## Utiliser un paquet externe

Exemple avec [Swiper](https://swiperjs.com)

Installer le paquet avec NPM

```
npm install swiper
```

Inclure le JS depuis un fichier JS

```js
import Swiper from "swiper";
```

Inclure la SCSS depuis un fichier SCSS

```SCSS
@import 'swiper/swiper'
```

## Exemple

Un exemple avec:

- Plusieurs fichiers JS
- Plusieurs fichiers SCSS
- Bootstrap
- Swiper
- Fontawesome

est disponible sur la branche _example_

```
git checkout example
```
